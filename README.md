# Mali (Data Transport)

## First step

This [GTFS](https://git.digitaltransport4africa.org/places/mali/raw/master/GTFS/GTFS.zip) was created from scratch by Emmanuel Bama ([@emmanuelbama](https://twitter.com/emmanuelbama)) and Mohamed Konaté ([@Mohamed_Konate_](https://twitter.com/Mohamed_Konate_)) during a one-week capacity building session organized by OGP Toolbox, Ecole des données and  Datactivist, thanks to funding from AFD ([more information here in French](https://forum.ogptoolbox.org/t/590)).

Both launched the project [Data Transport Mali](http://data-transport.org/) with the objective to collect and publish as open data a wide range of data about transports in Mali and in the western African region.

**Emmanuel Bama** founded the company [Billet Express](http://billetexpress.ml/)  which offers to travelers to book bus tickets online for 65 destinations in Mali and covering 7 other countries.


The tool [static-GTFS-manager](https://github.com/WRI-Cities/static-GTFS-manager), funded by WRI was used to build this GTFS.

_Documentation coming soon ([watch this space!](https://git.digitaltransport4africa.org/doc/data-transport-mali-GTFS))._

## Next step

Data collection with OpenStreetMap and GTFS built from OSM ([osm2gtfs](https://github.com/grote/osm2gtfs)).